
def apps = [
        ['name': 'app1','version': 'latest', 'envs': ['staging', 'prod'], 'cron_string': '* */1 * * *'],
        ['name': 'app2', 'version': 'latest', 'envs': ['staging', 'prod'], 'cron_string': '* */1 * * *'],
]

for (app in apps) {
    for (env in app['envs']) {
        pipelineJob("smokes-${app['name']}-${env}") {
            parameters {
                stringParam('version', app['version'], 'the version of the tests')
            }

            triggers {
                cron(app['cron_string'])
            }

            logRotator {
                numToKeep(30)
                daysToKeep(10)
            }

            definition{
                cpsScm {
                    scm {
                        git {
                            branches('smokes_pipeline')
                            remote {
                                url('https://gitlab.com/florian.neacsu/jenkins-starter.git')
                                credentials("gitlab_token")
                            }
                        }
                    }
                    scriptPath("./jobs/dsl/smokes.Jenkinsfile")
                }
            }
        }
    }
}
