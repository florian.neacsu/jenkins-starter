FROM jenkins/jenkins:lts
USER root


# install jenkins plugins
COPY jenkins-plugins.txt /usr/share/jenkins/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/plugins.txt

# copy jenkins jcasc cnfiguration
ENV CASC_JENKINS_CONFIG /var/jenkins_home/casc_configs
COPY jenkins.yaml ${CASC_JENKINS_CONFIG}/jenkins.yaml

ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false

VOLUME /var/jenkins_home