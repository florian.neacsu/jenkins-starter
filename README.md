# jenkins-starter

Jenkins as code starter repo

## How to configure Jenkins docker agents
The jenkins image contains the docker plugin needed, and it comes with JCasC configuration for docker slaves.
To enable communication to the docker host using the API change `ExecStart` in `/lib/systemd/system/docker.service` to:
`ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:4243 -H unix:///var/run/docker.sock`

The full tutorial can be found here: 
https://devopscube.com/docker-containers-as-build-slaves-jenkins/.

## How to run Jenkins locally:
docker run --rm -e GIT_ACCESS_TOKEN=dada873nkndjk2 -e AGENT_SSH_PASSWORD=jenkins -v /tmp/jenkins_home:/var/jenkins_home -p 8080:8080 jenkins-starter:v3
